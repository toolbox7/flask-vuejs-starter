----
tk notes

change app foder name from flask_login_tutorial to xapp

xapp : flask app, shft to backend API
    static -> ../vue_app/

vue_app: vue application
    all path is relative (easy deploy with prefix url, e.g /xyz/)
    ./css/xx
    ./js/xxx

may need to setup cors/flask?


----
tk vue.js without cli

https://dev.to/arswaw/create-a-lightweight-componentized-spa-without-node-569j
https://github.com/arswaw/VueSpaNONODE

https://steveolensky.medium.com/create-a-spa-vuejs-application-without-node-webpack-or-any-other-build-process-using-vue-router-b9bf8e36e958
https://markus.oberlehner.net/blog/goodbye-webpack-building-vue-applications-without-webpack/


----
tk simple http server with mime type setup (for javascript)
issue:
Failed to load module script: The server responded with a non-JavaScript MIME type of “text/plain”
fix:
   #Use to create local host
   import http.server
   import socketserver

   PORT = 1337

   Handler = http.server.SimpleHTTPRequestHandler
   Handler.extensions_map.update({
          ".js": "application/javascript",
   });

   httpd = socketserver.TCPServer(("", PORT), Handler)
   httpd.serve_forever()


fix2:
    ignore static_{url_path, folder}
    using send_file with
        1. sanity check (must be within restricted folder path)
        2. set proper mime type ('aplication/javascript')
        3. config multiple apps with different folder

----
tk setup apps
checkout repo to ./apps
    spa0 ->
        https://github.com/sobpilot/vuespa.git

    https://github.com/maoberlehner/goodbye-webpack-building-vue-applications-without-webpack.git
    https://github.com/arswaw/VueSpaNONODE.git


----
tk mysql package setup
mysql-connector-python 8.0.17
SQLAlchemy             1.3.8


----
tk bootstrap mdb design
https://github.com/mdbootstrap/Vue-Bootstrap-with-Material-Design
https://mdbootstrap.com/docs/vue/getting-started/download/


installation without npm (using cdn)
https://mdbootstrap.com/docs/vue/getting-started/quick-start/#cdn


