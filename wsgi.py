"""Application entry point."""
import logging as log

# from flask_login_tutorial import create_app

from backend import app

# from backend import create_app
# app = create_app()

if __name__ == "__main__":
    log.basicConfig(format='%(asctime)s|%(levelname)s|%(filename)s|%(funcName)s:%(lineno)s| %(message)s',
                    level=log.DEBUG)

    # app.run(host='0.0.0.0')
    app.run(host='127.0.0.1')
