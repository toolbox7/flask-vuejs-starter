
// import { NavbarTemplate } from '../templates/navbar-template.js'

// directly include
const NavbarTemplate = `<nav>

<ul>
<!--
  <li><a class="active" href="#/" ref="home">Home</a></li>
  <li><a href="#/about" ref="about">About</a></li>
-->
  <li><a href="#/"      ref="home" v-bind:class="{'active': active=='home'}"  v-on:click="active='home'" >Home</a></li>
  <li><a href="#/about" ref="about"v-bind:class="{'active': active=='about'}" v-on:click="active='about'">About</a></li>
</ul>

</nav>`;

const Navbar = {
  template: NavbarTemplate,
  data() {
    return {
      selections: [],
      active: "home",
    };
  },
  methods: {
    click(event) {
        console.log("click: ", event);
    }
  },
  mounted() {
    console.log("mounted init");
    this.selections = [
        this.$refs.home,
        this.$refs.about,
    ];
    console.log("selections: ", this.selections);
  }
}

export { Navbar }