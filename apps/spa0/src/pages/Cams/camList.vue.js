var spaCamList = Vue.component('CamList', {
  template: `<div>

    <div>
        <h3 class="display-2">{{ msg }}</h3>
    </div>

</div>`,
  props: ['title'],
  data() {
    return {
      msg: "hello camera",
    }
  },
  created() {
    console.log('created')
  },
  computed: {
  },
  methods: {
  }
})
