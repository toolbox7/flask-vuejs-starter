var spahome = Vue.component("Home", {
  template: `<div>
    <div>
        <h3 class="display-2">{{ msg }}</h3>
    </div>

    <img height="100%" width="100%" src="https://dsx.weather.com/util/image/map/airport_delays_1280x720.jpg"></img>
</div>`,
  props: ["title"],
  data() {
    return {
      msg: "hello, home"
    };
  },
  mounted() {
  },
  created() {
  },
  methods: {
  }
});
