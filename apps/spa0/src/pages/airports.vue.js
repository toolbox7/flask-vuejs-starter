var spaAirports = Vue.component("Airports", {
	template: `<div>
    <div>
        <h3 class="display-2">{{ msg }}</h3>
    </div>

    <div class="text-xs-center">
      <img height="70%" width="70%" src="https://dsx.weather.com/util/image/map/airport_delays_1280x720.jpg"></img>
    </div>

  </div>
</div>`,
	props: ["title"],
	created() {
	},
	watch: {
	},
	computed: {
	},
	data: function () {
		return {
		    msg: "hello, airport",
		};
	},
	mounted() {
	    console.log("mounted")
	},
	methods: {
	}
});