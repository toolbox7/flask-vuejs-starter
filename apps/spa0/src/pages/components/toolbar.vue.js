Vue.component("spa-toolbar", {
  template: `<div>
  Hello, spa-toolbar!

            <div
               v-for="item in items" :key="item.title" @click="$router.push(item.path)">
               {{ item.title }}
            </div

    <div style="margin-bottom: 30px;"></div>
</div>`,
  props: ["title", "email"],
  $_veeValidate: {
    validator: "new"
  },
  data() {
    return {
      menu: false,
      items: [
        { title: "Home", icon: "home", path: "/" },
        { title: "Airports", icon: "local_airport", path: "/airports" },
        { title: "Cameras", icon: "videocam", path: "/cams" }
      ]
    }
  },
  mounted() { },
  methods: {
  }
})
