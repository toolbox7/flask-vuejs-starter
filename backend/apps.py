"""Logged-in page routes."""
import logging as log
import os

from flask import Blueprint, render_template, redirect, url_for, request, send_file
from flask_login import current_user, login_required, logout_user


# Blueprint Configuration
apps_bp = Blueprint(
    'apps_bp', __name__,
    template_folder='templates',
    static_folder='static',
    url_prefix='/app'
)


# @apps_bp.route('/', methods=['GET'])
# @login_required
# def dashboard():
#     """Logged-in User Dashboard."""
#     return render_template(
#         'dashboard.jinja2',
#         title='Flask-Login Tutorial.',
#         template='dashboard-template',
#         current_user=current_user,
#         body="You are now logged in!"
#     )


# @apps_bp.route("/logout")
# @login_required
# def logout():
#     """User log-out logic."""
#     logout_user()
#     return redirect(url_for('auth_bp.login'))

@apps_bp.route("/")
# @login_required
def root():
    return "root of apps"


_base_dir = os.path.dirname(os.path.abspath(__file__)) + "/../apps"
_apps_map = {
    'spa': 'spa0/src',
    'spa_git': 'vuespa/src',
    'nonode': 'VueSpaNONODE',
    'goodbye': 'goodbye-webpack-building-vue-applications-without-webpack/src',
    'html': 'simple_html'
}


@apps_bp.route("/<path:file>")
# @login_required
def get_file(file):
    log.info("request url: {}".format(request.url))
    log.debug("apps base dir: {}".format(_base_dir))
    log.debug("get file: {}".format(file))

    app_name = file.split('/')[0]
    log.debug("app name: {}".format(app_name))

    app_dir = _apps_map[app_name]
    log.debug("app dir: {}".format(app_dir))

    app_file = _base_dir + os.path.sep + file.replace(app_name, app_dir, 1)
    log.debug("app file: {}".format(app_file))

    abs_file = os.path.abspath(app_file)
    log.debug("get absolute file: {}".format(abs_file))

    if os.path.isfile(abs_file) is not True:
        return "File not found", 404
    elif abs_file.endswith(".js"):
        log.debug("send as mime javascript")
        return send_file(abs_file, mimetype="application/javascript")
    else:
        return send_file(abs_file)
