"""Logged-in page routes."""
import logging as log

from flask import Blueprint, render_template, redirect, url_for, request, make_response, jsonify
# from flask import g
from flask_login import current_user, login_required, logout_user


# Blueprint Configuration
from .utils import pretty_json

main_bp = Blueprint(
    'main_bp', __name__,
    template_folder='templates',
    static_folder='static'
)


@main_bp.route('/', methods=['GET'])
@login_required
def dashboard():
    """Logged-in User Dashboard."""
    return render_template(
        'dashboard.jinja2',
        title='Flask-Login Tutorial.',
        template='dashboard-template',
        current_user=current_user,
        body="You are now logged in!"
    )


@main_bp.route("/logout")
@login_required
def logout():
    """User log-out logic."""
    logout_user()
    return redirect(url_for('auth_bp.login'))


@main_bp.route("/hello")
# @login_required
def hello():
    # headers = {"Content-Type": "application/json"}
    # return make_response(jsonify({
    #     "hello": "world",
    # }), 200, headers)

    return make_response(jsonify({
        "hello": "world",
    }), 200)


@main_bp.route("/request", methods=["GET", "PUT", "POST", "DELETE"])
# @login_required
def dump_request():
    resp = {
        "endpoint": request.endpoint,
        "method": request.method,
        "cookies": request.cookies,
        "remote_addr": request.remote_addr,
        "headers": request.headers,

        "data": request.data,
        "args": request.args,
        "form": request.form,
        "values": request.values,
        "file": request.files,
        "json": request.json,

        "request": request,
    }
    headers = {"Content-Type": "application/json"}
    return make_response(pretty_json(resp), 200, headers)


def site_map():
    output = {}

    # for rule in g.url_map.iter_rules():
    from . import app
    for rule in app.url_map.iter_rules():
        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        try:
            url = url_for(rule.endpoint, **options)
        except:
            log.error("except in url_for: {}".format(rule.endpoint))
        output[str(rule.endpoint)] = (str(url), str(rule.methods))

    return output


@main_bp.route('/misc/site_map', methods=['GET'])
@main_bp.route('/misc/sitemap', methods=['GET'])
@main_bp.route('/sitemap', methods=['GET'])
def get_site_map():
    output = site_map()
    return jsonify(output)


@main_bp.route('/misc/endpoints', methods=['GET'])
@main_bp.route('/endpoints', methods=['GET'])
def get_endpoints():
    output = site_map()
    s = "endpoints: "
    for k,v in output.items():
        s +=  "\n{:25s} {:20s} {}".format(v[0], v[1], k)
    return s

