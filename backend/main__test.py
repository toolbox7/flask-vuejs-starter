import logging as log
import mimetypes
import random
import os
import requests

import unittest

from sqlalchemy import create_engine

from . import create_app, db, login_manager, app
from .models import User
from .utils import pretty_json


class MainAppUtilAndTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        log.basicConfig(format='%(asctime)s|%(levelname)s|%(filename)s|%(funcName)s:%(lineno)s| %(message)s',
                        level=log.DEBUG)

    def test_something(self):
        self.assertEqual(True, False)

    # disable dangerous test (change/modify system)
    def __test_init_db_and_create_new_user(self):
        # app = create_app()
        with app.app_context():
            db.metadata.create_all(db.engine)
            users = User.query.all()
            if users:
                log.info('Users already exists!')
                for i, u in enumerate(users):
                    log.info("user #{} : {}".format(i, u))
            else:
                log.info('No user exists!')

            id = random.randint(1000, 9999)
            user = User(
                name="james_{}".format(id),
                email="dummy@none_{}".format(id)
            )
            user.set_password("password")
            db.session.add(user)
            db.session.commit()
            log.info("user added: {}".format(user))
        log.info("done")

    def test_list_users(self):
        # app = create_app()
        with app.app_context():
            db.metadata.create_all(db.engine)
            users = User.query.all()
            if users:
                log.info('Users already exists!')
                for i, u in enumerate(users):
                    log.info("user #{} : {}".format(i, u))
                    log.info("user {}: {}/{}/{}/{}".format(i, u.name, u.email, u.website, u.password))
            else:
                log.info('No user exists!')
        log.info("done")

    def test_sqlite_meta_tables(self):
        log.info("current dir: {}".format(os.getcwd()))
        path = os.path.dirname(os.path.abspath(__file__)).replace("\\", '/')
        log.info("file path: {}".format(path))
        # engine = create_engine('sqlite:///{}//db.sqlite'.format(path))
        engine = create_engine('sqlite:///{}//db.sqlite'.format(path))
        for i, n in enumerate(engine.table_names()):
            log.info("table #{}: {}".format(i, n))
        log.info("done")

    def test_digging(self):
        with app.app_context():
            log.info("db.metadata: {}".format(db.metadata))
            log.info("db.metadata.tables: {}".format(db.metadata.tables))
            log.info("app.url_map: {}".format(app.url_map))
            log.info("app.blueprints: {}".format(app.blueprints))

        log.info("done")

    @unittest.skip("dangerous, only for manually run")
    def test_skip_ignore(self):
        log.info("done")

    def test_mimetypes(self):
        file = "apps.py"
        tp = mimetypes.guess_type(file)
        log.info("file {} type: {}".format(file, tp))

        file = "main.js"
        tp = mimetypes.guess_type(file)
        log.info("file {} type: {}".format(file, tp))

    def test_post_json_params(self):
        data = {'sender': 'Alice',
                'receiver': 'Bob',
                'message': 'We did it!'}
        resp = requests.post("http://localhost:5000/request?Hello=World", json={'json_payload': data})
        log.info("resp: {}".format(resp))
        log.info("resp.text: {}".format(resp.text))
        log.info("resp.content: {}".format(resp.content))
        log.info("resp.cookies: {}".format(resp.cookies))
        log.info("resp.headers: {}".format(resp.headers))
        log.info("resp.json(): \n{}".format(pretty_json(resp.json())))

    def test_remote_http_dump(self):
        log.info("""r = requests.post('http://httpbin.org/post?Hello=World', json={"key": "value"})""");
        r = requests.post('http://httpbin.org/post?Hello=World', json={"key": "value"})
        log.info("resp staus_code: {}".format(r.status_code));
        log.info("resp json: {}".format(pretty_json(r.json())))


if __name__ == '__main__':
    unittest.main()
