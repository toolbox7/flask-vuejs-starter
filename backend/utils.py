import json


class PrettyEncoder(json.JSONEncoder):
    def default(self, obj):
        try:
            return json.JSONEncoder.default(self, obj)
        except:
            try:
                return json.JSONEncoder.default(self, dict(**obj))
            except:
                return str(obj)


def pretty_json(obj):
    return json.dumps(dict(**obj), cls=PrettyEncoder, indent=4)

