PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "flasklogin-users" (
	id INTEGER NOT NULL, 
	name VARCHAR(100) NOT NULL, 
	email VARCHAR(40) NOT NULL, 
	password VARCHAR(200) NOT NULL, 
	website VARCHAR(60), 
	created_on DATETIME, 
	last_login DATETIME, 
	PRIMARY KEY (id), 
	UNIQUE (email)
);
INSERT INTO "flasklogin-users" VALUES(1,'james','dummy@none','sha256$v5ob6zrI$a59d959e787e208d3a81092727156b36c67c01a646556b4d46dc3135c41f287c',NULL,NULL,NULL);
INSERT INTO "flasklogin-users" VALUES(2,'james_4996','dummy@none_4996','sha256$sY5eKLWI$2293f462331f479d80b2a1a8147081b8ce496a400e5d7a0686646fdb41d6603d',NULL,NULL,NULL);
INSERT INTO "flasklogin-users" VALUES(3,'james_4507','dummy@none_4507','sha256$pwx5i0bh$47da9f2c995d3b603730e5c8771da192b436d6560ca4f0c2f55f474663e52b27',NULL,NULL,NULL);
INSERT INTO "flasklogin-users" VALUES(4,'james_4467','dummy@none_4467','sha256$UuaUbqAy$87364ae4cf6340da76602307618ce67896f4377fbb12cc1b6620231f6eb2f181',NULL,NULL,NULL);
INSERT INTO "flasklogin-users" VALUES(5,'James','james.xi@nexusenergycanada.com','sha256$6JYO1CrQ$96098f2c2d148ae846e1a98cec26f617cd923d279a4b7d416a8be30b6db8acd6','openpathfinder.com',NULL,NULL);
INSERT INTO "flasklogin-users" VALUES(6,'james_9892','dummy@none_9892','sha256$23h71ke5$6fbd3a94fbdfbd301accd00382bd8ce29a37139320d7b0e8ea4f2a2db1461ec1',NULL,NULL,NULL);
INSERT INTO "flasklogin-users" VALUES(7,'Yongjian Xi','xiyongjian@gmail.com','sha256$YXePdwgv$50d893da150f7f53cb779d28d259f7066ea290530aa6ac3e8d416d497259a292','kk',NULL,NULL);
INSERT INTO "flasklogin-users" VALUES(8,'James','james@nexus.com','sha256$iuaJCTiU$5eaacb9b250798ecd3c2156709a30e2b1c7029f4f459de6be74347b41a752d73','askl.j',NULL,NULL);
COMMIT;
