"""Logged-in page routes."""
import logging as log
import os

from flask import Blueprint, render_template, redirect, url_for, request, send_file, jsonify
from flask_login import current_user, login_required, logout_user, login_user

from .models import User

# Blueprint Configuration
api_bp = Blueprint(
    'api_bp', __name__,
    template_folder='templates',
    static_folder='static',
    url_prefix='/api'
)


@api_bp.route("/")
def root():
    log.info("hit /api/")
    return "hit /api/"


@api_bp.route("/login", methods=['GET', 'POST'])
def login():
    log.info("hit /api/login")

    payload = request.json

    username = payload.get("username")
    password = payload.get("password")
    log.info("login with username {}, password {}".format(username, password))

    user = User.query.filter_by(name=username).first()
    if user and user.check_password(password=password):
        login_user(user)
        log.info("login user, success")
        return jsonify({
            "status": "success",
            "username": username
        })
    else:
        log.info("login user, failed")
        return jsonify({
            "status": "error",
            "username": "guest"
        })
